import { expect } from 'chai';

import async from '../../src/index';

function timer(sleep: number) {
    if (sleep <= 0) return Promise.reject(new Error('A positive sleep time is needed!'));
    return async.delay(sleep).then(() => new Date().getTime());
}
function timerFilter(sleep: number) {
    return async.delay(sleep).then(() => sleep > 600);
}

describe('async', () => {
    it('should delay', async () => {
        const start = new Date().getTime();
        const end = await async.delay(500).then(() => new Date().getTime());
        expect(end).to.be.greaterThan(start + 499);
    });
    it('should retry', async () => {
        const end = new Date().getTime() + 1200;
        const done = await async.retry(async () => {
            const now = new Date().getTime();
            if (now < end) return Promise.reject(new Error('to early'));
            return now;
        });
        expect(done).to.be.greaterThan(end - 1);
    });
    it('should fail on retries', async () => {
        const end = new Date().getTime() + 1200;
        try {
            await async.retry(async () => {
                const now = new Date().getTime();
                if (now < end) return Promise.reject(new Error('to early'));
                return now;
            }, 100, 3);
        } catch (err) {
            return;
        }
        throw new Error('Call should fail on this values.');
    });
    it('should run forever', async () => {
        const end = new Date().getTime() + 1200;
        try {
            await async.forever(async () => {
                const now = new Date().getTime();
                if (now > end) return Promise.reject(new Error('break'));
            });
        } catch (error) {
            expect(error).to.be.instanceOf(Error);
        }
    });
    it('should run multiple times', async () => {
        const start = new Date().getTime();
        const done = await async.times(() => timer(100), 100, 5);
        expect(done).to.be.greaterThan(start);
    });
    it('should run whilst', async () => {
        const start = new Date().getTime();
        const end = new Date().getTime() + 400;
        const done = await async.whilst(
            () => timer(100),
            async () => {
                const now = new Date().getTime();
                if (now > end) return Promise.reject(new Error('time out'));
                return now;
            }
        );
        expect(done).to.be.greaterThan(start);
    });
    it('should run until', async () => {
        const start = new Date().getTime();
        const end = new Date().getTime() + 400;
        const done = await async.until(
            () => timer(100),
            async () => {
                const now = new Date().getTime();
                if (now < end) return Promise.reject(new Error('in time'));
                return now;
            }
        );
        expect(done).to.be.greaterThan(start);
    });
    it('should run each', async () => {
        const list = [800, 500, 200, 800, 100];
        await async.each(list, timer);
    });
    it('should run each limit', async () => {
        const list = [800, 500, 200, 800, 100];
        await async.each(list, timer, 3);
    });
    it('should run map', async () => {
        const list = [800, 500, 200, 800, 100];
        const res = await async.map(list, timer);
        //        console.log(res);
        expect(res[0] + 1).to.be.gte(res[3]);
    });
    it('should fail on map', async () => {
        const list = [800, 500, 0, 800, 100];
        try {
            await async.map(list, timer, 3);
        } catch (err) {
            return;
        }
        throw new Error('Call should fail on this values.');
    });
    it('should run map limit', async () => {
        const list = [800, 500, 200, 800, 100];
        const res = await async.map(list, timer, 3);
        //        console.log(res);
        expect(res[0] + 1).to.be.lt(res[3]);
    });
    it('should run mapObject', async () => {
        const obj = { a: 800, b: 500, c: 200, d: 800, e: 100 };
        const res = await async.mapObject(obj, timer);
        // console.log(res);
        expect(res.a + 1).to.be.gte(res.c);
    });
    it('should run filter', async () => {
        const list = [800, 500, 200, 810, 100];
        const res = await async.filter(list, timerFilter);
        //        console.log(res);
        expect(res.length).to.be.equal(2);
    });
    it('should run filter limit', async () => {
        const list = [800, 500, 200, 810, 100];
        const res = await async.filter(list, timerFilter, 3);
        //        console.log(res);
        expect(res.length).to.be.equal(2);
    });
    it('should run reject', async () => {
        const list = [800, 500, 200, 810, 100];
        const res = await async.reject(list, timerFilter);
        //        console.log(res);
        expect(res.length).to.be.equal(3);
    });
    it('should run any', async () => {
        const list = [800, 500, 200, 810, 100, 900];
        const res = await async.any(list, timerFilter, 2);
        expect(res).to.be.equal(800);
    });
    it('should run all', async () => {
        const list = [800, 500, 200, 810, 100, 900];
        const res = await async.all(list, timerFilter, 3);
        expect(res).to.be.equal(false);
    });
    it('should run none', async () => {
        const list = [800, 500, 200, 810, 100, 900];
        const res = await async.none(list, timerFilter, 3);
        expect(res).to.be.equal(false);
    });
    it('should run parallel', async () => {
        const list = [() => Promise.resolve(5), () => Promise.resolve(8)];
        const res = await async.parallel(list);
        expect(res.length).to.be.equal(2);
    });
    it('should run serial', async () => {
        const list = [() => Promise.resolve(5), () => Promise.resolve(8)];
        const res = await async.serial(list);
        expect(res.length).to.be.equal(2);
    });
});
