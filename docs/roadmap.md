# Roadmap

It's not a clear roadmap here, but more a list of possible extension ways divided into thematic groups. If you need a specific extension or change contact me. Or send me a pull request.

## Further Ideas

- [ ] p-queue  6.6.2  ❯  7.1.0 - ESM Problem - TypeError [ERR_UNKNOWN_FILE_EXTENSION]: Unknown file extension ".ts" for /home/alex/code/node-async/test/mocha/index.ts

new:

-   reduce - calling with array and start value returning one last value

control:

-   queue - dynamic queue running while not empty
-   priorityQueue - with additional priorities for each job
-   depend - Determines the best order for running the AsyncFunctions in tasks, based on their requirements.
-   waterfall - Each function consumes the return value of the function before.
-   race - Runs the tasks array of functions in parallel, without waiting until the previous function has completed.

{!docs/assets/abbreviations.txt!}
