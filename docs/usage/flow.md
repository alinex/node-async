title: Flow

# Managing control flow

## delay

!!! definition

    Delay the further execution for a given time range.

    Parameter:

    - `time` Numer of milliseconds to wait.
    - `value` Value to be given forward as Promise.

    ```ts
    async.delay<T>(time: number, value?: T): Promise<T>
    ```

In the following the time is returned after 500 milliseconds of sleep time.

```ts
const endDate = await async.delay(500).then(() => new Date().getTime());
```

## retry

This allows you to retry a function if it failed.

!!! definition

    Retry the given function multiple times if it is rejected.

    Parameter:

    - `fn` Function to be called without arguments.
    - `sleep` Time to wait in milliseconds between each try.
    - `tries` Maximum number of tries before.

    ```ts
    async.retry<T>(
        fn: () => Promise<T>,
        sleep = 500,
        tries = 5
    ): Promise<T>
    ```

This can look like:

```ts
const end = new Date().getTime() + 1200;
const done = await async.retry(async () => {
    const now = new Date().getTime();
    if (now < end) return Promise.reject(new Error('to early'));
    return now;
});
```

The above function will have to make 3 tries till the date dependent test function will return true.

## forever

This allows you to run a function till it fails.

!!! definition

    Retry the given function till it rejects.

    Parameter:

    - `fn` Function to be called without arguments.

    ```ts
    async.forever<T>(
        fn: () => Promise<T>
    ): Promise<T>
    ```

This can look like:

```ts
const end = new Date().getTime() + 1200;
await async.forever(async () => {
    const now = new Date().getTime();
    if (now > end) return Promise.reject(new Error('end now'));
});
```

The above function will run for 1200 ms, till it fails.

## times

This allows you to run a function multiple times.

!!! definition

    Run the given function multiple times.

    Parameter:

    - `fn` Function to be called without arguments.
    - `sleep` Time to wait in milliseconds between each try.
    - `num` Number of runs.

    ```ts
    async.times<T>(
        fn: () => Promise<T>,
        sleep = 500,
        num = 5
    ): Promise<T>
    ```

This can look like:

```ts
function timer(sleep: number) {
    if (sleep <= 0) return Promise.reject(new Error('A positive sleep time is needed!'));
    return async.delay(sleep).then(() => new Date().getTime());
}
const done = await async.times(() => timer(100), 100, 5);
```

The above function will run 5 times (waiting 100ms) with additional 100ms pause between each call.
The result of the last call is returned.

## whilst / doWhilst

This allows you to run a while the given asynchronous function returns a `true` value. `doWhilst` will check at the end making it run at least once.

!!! definition

    Run the given function while async check is true.

    Parameter:

    - `fn` Function to be called without arguments.
    - `check` Return `true` value to go on.
    - `sleep` Time to wait in milliseconds between each run.

    ```ts
    async.whilst<T>(
        fn: () => Promise<T>,
        check: () => Promise<any>,
        sleep = 0
    ): Promise<T>
    ```

    ```ts
    async.doWhilst<T>(
        fn: () => Promise<T>,
        check: () => Promise<any>,
        sleep = 0
    ): Promise<T>
    ```

This can look like:

```ts
const end = new Date().getTime() + 400;
const done = await async.whilst(
    () => timer(100), // method to call
    async () => {
        // check
        const now = new Date().getTime();
        if (now > end) return Promise.reject(new Error('time out'));
        return now;
    }
);
```

The above function will run 4 times (needing 100ms each run) without pause between each call.
The result of the last call is returned.

## until / doUntil

This allows you to run a while the given asynchronous function returns a `true` value. `doUntil` will check at the end making it run at least once.

!!! definition

    Run the given function until async check is true.

    Parameter:

    - `fn` Function to be called without arguments.
    - `check` Return `true` value to stop.
    - `sleep` Time to wait in milliseconds between each run.

    ```ts
    async.until<T>(
        fn: () => Promise<T>,
        check: () => Promise<any>,
        sleep = 0
    ): Promise<T>
    ```

    ```ts
    async.doUntil<T>(
        fn: () => Promise<T>,
        check: () => Promise<any>,
        sleep = 0
    ): Promise<T>
    ```

This can look like:

```ts
const end = new Date().getTime() + 400;
const done = await async.until(
    () => timer(100), // method to call
    async () => {
        // check
        const now = new Date().getTime();
        if (now < end) return Promise.reject(new Error('in time'));
        return now;
    }
);
```

The above function will run 4 times (needing 100ms each run) without pause between each call.
The result of the last call is returned.

## parallel

This will run multiple asynchronous functions with the ability to limit concurrency.

!!! definition

    Calls a set of provided functions in parallel.

    Parameter:

    -   `list` A list of async function callbacks to invoke. The callback takes
        no arguments and resolves to a void.
    -   `concurrency` Limits the maximum number of functions to run concurrently.

    ```ts
    async.parallel<T = any>(
        list: { (): Promise<T> }[],
        concurrency?: number
    ): Promise<T[]>
    ```

You may run multiple functions like:

```ts
const list = [() => Promise.resolve(5), () => Promise.resolve(8)];
const res = await async.parallel(list);
// res = [
//   5,
//   8
// ]
```

**Run in series:** This is possible by setting `concurrency=1`, that's the same as calling `serial()`.

## serial

This will run multiple asynchronous functions in order, each after the other.

!!! definition

    Calls a set of provided functions in series.

    Parameter:

    -   `list` A list of async function callbacks to invoke. The callback takes
        no arguments and resolves to a void.

    ```ts
    async.serial<T = any>(list: { (): Promise<T> }[]): Promise<T[]>
    ```

You may run multiple functions like:

```ts
const list = [() => Promise.resolve(5), () => Promise.resolve(8)];
const res = await async.serial(list);
// res = [
//   5,
//   8
// ]
```

{!docs/assets/abbreviations.txt!}
