title: Collections

# Working with collections

## each

This will work like `Array.forEach()` but using asynchronous functions and with the ability to limit concurrency. It's nearly the same as running `map()` but without returning amy results.

!!! definition

    Calls a provided function once per input in parallel like `forEach`.

    Parameter:

    -   `list` A list of input elements to iterate.
    -   `fn` An async function callback invoked for each element in the list.

        This callback takes three arguments:

        - the current element being processed,
        - the index of the current element,
        - and the input list.

    - `concurrency` Limits the maximum number of functions to run concurrently.

    ```ts
    async.each<T>(
        list: T[],
        fn: { (value: T, index?: number, list?: T[]): Promise<any> },
        concurrency?: number
    ): Promise<void>
    ```

You may run the method like this:

```ts
function timer(sleep: number) {
    return async.delay(sleep).then(() => new Date().getTime());
}

const list = [800, 500, 200, 800, 100];
await async.each(list, timer, 3);
// takes about one second
```

This will run at most three timer functions in parallel. Without the limit all will be started in parallel.

**Run in series:** This is possible by setting `concurrency=1`.

**Iterate over object:** Iterate over `Object.keys(<name>)` and read the value in your function from the object using the given key.

## map

This will work like `Array.map()` but using asynchronous functions and with the ability to limit concurrency. It's nearly the same as running `each()` but with returning the results.

!!! definition

    Creates a new array with the results of calling a provided function in parallel on every input.
    The output will be in the same order as the input.

    Parameter:

    -   `list` A list of input elements to map.
    -   `fn` An async function callback that produces an element of the output list.

        The callback takes three arguments:#

        - the current element being processed,
        - the index of the current element,
        - and the input list.

        The callback resolves to a single output element.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return: A list of mapped elements in the same order as the input.

    ```ts
    async.map<T1, T2>(
        list: T1[],
        fn: { (value: T1, index?: number, list?: T1[]): Promise<T2> },
        concurrency?: number
    ): Promise<T2[]>
    ```

You may run the method like this:

```ts
function timer(sleep: number) {
    return async.delay(sleep).then(() => new Date().getTime());
}

const list = [800, 500, 200, 800, 100];
const res = await async.map(list, timer, 3);
// res = [
//   1561907798414,
//   1561907798114,
//   1561907797813,
//   1561907798614,
//   1561907798214
// ]
```

This will run at most three timer functions in parallel. Without the limit all will be started in parallel.

**Run in series:** This is possible by setting `concurrency=1`.

## mapObject

This will work like `map()` above, but working on objects instead of arrays. The provided function will be called for each key of the given object.

!!! definition

    Creates a new object with the results of calling a provided function in parallel on every input element.
    The output will be a shallow clone.

    Parameter:

    -   `obj` An object as input map.
    -   `fn` An async function callback that produces an element of the output object.

        The callback takes three arguments:#

        - the current element being processed,
        - the key of the current element,
        - and the input object.

        The callback resolves to a single output element.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return: An object like the input but with the new key values.

    ```ts
    async.mapObject(
        obj: { [key: string]: any },
        fn: { (value: any, key: string, obj?: {}): Promise<any> },
        concurrency?: number
    ): Promise<{ [key: string]: any }>
    ```

You can run a method for each key:

```ts
function timer(sleep: number) {
    return async.delay(sleep).then(() => new Date().getTime());
}

const obj = { a: 800, b: 500, c: 200, d: 800, e: 100 };
const res = await async.mapObject(obj, timer, 3);
// res = {
//   a: 1561907798414,
//   b: 1561907798114,
//   c: 1561907797813,
//   d: 1561907798614,
//   e: 1561907798214
// }
```

This will run at most three timer functions in parallel. Without the limit all will be started in parallel.

**Run in series:** This is possible by setting `concurrency=1`.

## filter

This will work like `Array.filter()` but using asynchronous functions and with the ability to limit concurrency.

!!! definition

    Creates a new array with all elements that pass the test implemented by the
    provided async function.
    The output will be in the same order as the input.

    Parameter:

    -   `list` A list of input elements to test.
    -   `fn` An async function callback invoked for each element in the list.

        The callback takes three arguments:

        - the current element being processed,
        - the index of the current element,
          - and the input list.

        The callback resolves to true for elements to be included in the output list.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return:  A list of filtered elements in the same order as the input.

    ```ts
    async.filter<T>(
        list: T[],
        fn: { (value: T, index?: number, list?: T[]): Promise<boolean> },
        concurrency?: number
    ): Promise<T[]>
    ```

You may run the method like this:

```ts
function timerFilter(sleep: number) {
    return async.delay(sleep).then(() => sleep > 600);
}

const list = [800, 500, 200, 800, 100];
const res = await async.filter(list, timerFilter, 3);
// res = [
//   800,
//   800
// ]
```

## reject

This will work like `async.filter()` but will return all elements for which the async truth test is false.

!!! definition

    Creates a new array with all elements that fail the truth test of
    the provided async function.
    The output will be in the same order as the input.

    Parameter:

    -   `list` A list of input elements to test.
    -   `fn` An async function callback invoked for each element in the list.

        The callback takes three arguments:

        - the current element being processed,
        - the index of the current element,
          - and the input list.

        The callback resolves to false for elements to be included in the output list.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return:  A list of filtered elements in the same order as the input.

    ```ts
    async.reject<T>(
        list: T[],
        fn: { (value: T, index?: number, list?: T[]): Promise<boolean> },
        concurrency?: number
    ): Promise<T[]>
    ```

You may run the method like this:

```ts
function timerFilter(sleep: number) {
    return async.delay(sleep).then(() => sleep > 600);
}

const list = [800, 500, 200, 800, 100];
const res = await async.reject(list, timerFilter, 3);
// res = [
//   500,
//   200,
//   100
// ]
```

## any

This will work like `filter()` but only returning the element which passed the function as first. This is a short circuit method, meaning after the result is found no more async calls will be made.

!!! definition

    Returns the first element that pass the test implemented by the
    provided async function.

    Parameter:

    -   `list` A list of input elements to test.
    -   `fn` An async function callback invoked for each element in the list.

        The callback takes three arguments:

        - the current element being processed,
        - the index of the current element,
          - and the input list.

        The callback resolves to true for elements to be included in the output list.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return:  The element value, which succeeded as first.

    ```ts
    async.any<T>(
        list: T[],
        fn: { (value: T, index?: number, list?: T[]): Promise<boolean> },
        concurrency?: number
    ): Promise<T>
    ```

You may run the method like this:

```ts
function timerFilter(sleep: number) {
    return async.delay(sleep).then(() => sleep > 600);
}

const list = [800, 500, 200, 810, 100];
const res = await async.any(list, timerFilter, 3);
// res = 800
// test for 810 is already running as 800 returns, but test for 100 is neither done
```

## all

This will return `true` if all elements succeeded the passed function. Instead of checking
the `filter(()` result this method has a short circuit method, meaning after the result is
found no more async calls will be made.

!!! definition

    Returns `true` if all elements pass the test implemented by the
    provided async function.

    Parameter:

    -   `list` A list of input elements to test.
    -   `fn` An async function callback invoked for each element in the list.

        The callback takes three arguments:

        - the current element being processed,
        - the index of the current element,
          - and the input list.

        The callback resolves to true for elements to be included in the output list.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return:  boolean

    ```ts
    async.all<T>(
        list: T[],
        fn: { (value: T, index?: number, list?: T[]): Promise<boolean> },
        concurrency?: number
    ): Promise<T>
    ```

You may run the method like this:

```ts
function timerFilter(sleep: number) {
    return async.delay(sleep).then(() => sleep > 600);
}

const list = [800, 500, 200, 810, 100];
const res = await async.all(list, timerFilter, 3);
// res = false
// test for 200 fails, so the last two test won't be called
```

## none

This will return `true` if no elements succeeded the passed function. Instead of checking
the `filter(()` result this method has a short circuit method, meaning after the result is
found no more async calls will be made.

!!! definition

    Returns `true` if no elements pass the test implemented by the
    provided async function.

    Parameter:

    -   `list` A list of input elements to test.
    -   `fn` An async function callback invoked for each element in the list.

        The callback takes three arguments:

        - the current element being processed,
        - the index of the current element,
          - and the input list.

        The callback resolves to true for elements to be included in the output list.

    -   `concurrency` Limits the maximum number of functions to run concurrently.

    Return:  boolean

    ```ts
    async.none<T>(
        list: T[],
        fn: { (value: T, index?: number, list?: T[]): Promise<boolean> },
        concurrency?: number
    ): Promise<T>
    ```

You may run the method like this:

```ts
function timerFilter(sleep: number) {
    return async.delay(sleep).then(() => sleep > 600);
}

const list = [800, 500, 200, 810, 100];
const res = await async.none(list, timerFilter, 2);
// res = false
// test for 800 fails, so the last test won't be called
```

{!docs/assets/abbreviations.txt!}
