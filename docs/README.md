title: Usage

# Async Helper

![core icon](https://assets.gitlab-static.net/uploads/-/system/project/avatar/13521754/async-icon.png){: .right .icon}

Multiple methods to make working with promises easier.

## Installation

To include this in your module install it first:

```bash
npm install @alinex/async --save
```

Now only load it in your code and call it's methods.

## Load Module

Load the methods you need:

=== "TypeScript"

    ```ts
    // load default functions
    import async from '@alinex/async';
    // load all functions
    import * as async from '@alinex/async';
    // load specific function into own variables
    import { retry } from '@alinex/async';
    ```

=== "JavaScript"

    ```js
    // load only the default functions
    const async = require('@alinex/async').default;
    // load all functions
    const async = require('@alinex/async');
    ```

## Methods

1. [Managing control flow](usage/flow.md):

    - `delay` - Delay the further execution for a given time range.
    - `retry` - Retry the given function multiple times if it is rejected.
    - `forever` - Run the given function till it is rejected.
    - `times` - run given number of times (in series).
    - `whilst`/`doWhilst` - run `fn` while expression is true (in series).
    - `until`/`doUntil` - like whilst but executing while it is false.
    - `parallel`/`serial` - Calls a set of provided functions in parallel or serial.

2. [Working with collections](usage/collection.md):

    - `each` - Calls a provided function once per input in parallel like `forEach`.
    - `map`/`mapObject` - Creates a new array/object with the results of calling a provided function in parallel on every input element.
    - `filter`/`reject` - Creates a new array with all elements that pass the test implemented by the provided function in parallel.
    - `any` - Returns the first element which succeeds the asynchronous truth test and stops running more tests after first found.
    - `all`/`none` - Returns `true` if all or no element succeeds the asynchronous truth test.

All of them will shortcut the execution on the first rejection within an asynchronous call.

## Debugging

Like in all my modules the [debug](https://www.npmjs.com/package/debug) module is included. See the also the [my documentation](https://alinex.gitlab.io/node-core/standard/debug/) on how to use and control it.

The async module uses the following names:

-   `async:*` - to get all
-   `async:delay` - showing start and end of delay
-   `async:retry` - showing each retry round, counting down
-   `async:map` - showing each start and end of the processing with parameter and result (also used for the `each`, `mapObject` method)
-   `async:filter` - showing each start and end of the processing with parameter and result (also used for the `reject` method)
-   `async:parallel` - showing each start and end of the processing with result (also used for `serial` method)

## Support

I don't give any paid support but you may create [GitLab Issues](https://gitlab.com/alinex/node-async/-/issues):

-   Bug Reports or Feature: Please make sure to give as much information as possible. And explain how the system should behave in your opinion.
-   Code Fixing or Extending: If you help to develop this package I am thankful. Develop in a fork and make a merge request after done. I will have a look at it.
-   Should anybody be willing to join the core team feel free to ask, too.
-   Any other comment or discussion as far as it is on this package is also kindly accepted.

Please use for all of them the [GitLab Issues](https://gitlab.com/alinex/node-async/-/issues) which only requires you to register with a free account.

{!docs/assets/stats-pdf-license.txt!}

{!docs/assets/abbreviations.txt!}
