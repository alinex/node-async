# Last Changes

## Unreleased Changes

## Version 1.3.0 (08.05.2021)

-   upgrade to ES2019, Node >= 12

-   Hotfix 1.3.1 (09.05.2021) - fix typescript settings

## Version 1.2.0 (07.01.2020)

-   Updated modules
-   Better debugging for rejected promises.
-   Add any which returns the first value in coll that passes an async truth test.
-   Add all which returns true if all elements in coll satisfies an async test.
-   Add none which returns true if none of the elements in coll satisfies an async test.
-   Add `whilst`/`doWhilst` to run `fn` while expression is true (in series).
-   Add `until`/`doUntil` working like whilst but executing while it is false.
-   Add `forever` to run the given function till it is rejected.

Fixes:

-   Update 1.2.1 (15.02.2020) - update package management
-   Hotfix 1.2.2 (10.07.2020) - add short circuit to async.filter/reject instead of unhandledRejection
-   Hotfix 1.2.3 (17.08.2020) - add short circuit to async.map/mapObject for empty arrays
-   Hotfix 1.2.4 (10.09.2020) - fix multiple rejection problem
-   Update 1.2.5 (23.10.2020) - update packages and doc structure
-   Update 1.2.6 (02.01.2021) - update packages and new doc theme
-   Hotfix 1.2.7 (22.03.2021) - bug: retry is not returning error after all tries exceeded
-   Hotfix 1.2.8 (22.03.2021) - bug: retry fix returning error object as it is

## Version 1.1.0 (03.08.2019)

-   Extended debug abilities with full support on all methods
-   Add `serial()` as alias to `parallel()` with concurrency 1
-   Add `mapObject` to create a new object with the results of calling a provided function in parallel on every input element.
-   Add `reject` as the opposite of filter. Removes values that failed in an async truth test.

Hotfix Version 1.1.1 fixes the main path in package.json.

Hotfix Version 1.1.2 make some parameters optional in TypeScript definitions.

## Version 1.0.0 (02.08.2019)

Methods extracted from [alinex core](https://alinex.gitlab.io/node-core) containing:

-   `delay` - Delay the further execution for a given time range.
-   `retry` - Retry the given function multiple times if if it is rejected.
-   `each` - Calls a provided function once per input in parallel like `forEach`.
-   `map` - Creates a new array with the results of calling a provided function in parallel on every input.
-   `filter` - Creates a new array with all elements that pass the test implemented by the provided function in parallel.
-   `parallel` - Calls a set of provided functions in parallel.

{!docs/assets/abbreviations.txt!}
