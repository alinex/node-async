// Async Helper
// =================================================
// A collection of async handler working with promises.

import Debug from 'debug';
import pLimit from 'p-limit';
import pQueue from 'p-queue';

const debugDelay = Debug('async:delay');
const debugRetry = Debug('async:retry');
const debugMap = Debug('async:map');
const debugFilter = Debug('async:filter');
const debugParallel = Debug('async:parallel');

/**
 * Delay the further execution for a given time range.
 * @param time Numer of milliseconds to wait.
 * @param value Value to be given forward as Promise.
 */
export function delay<T>(time: number, ...value: T[]): Promise<T> {
  return new Promise(function (resolve) {
    if (time === 0) return resolve.bind(null, ...value)();
    debugDelay('starting delay of %d ms', time);
    setTimeout(() => {
      debugDelay('going on after %s ms', time);
      resolve.bind(null, ...value)();
    }, time);
  });
}

/**
 * Retry the given function multiple times if if it is rejected.
 * @param fn Function to be called.
 * @param sleep Time to wait in milliseconds between each try.
 * @param tries Maximum number of tries before.
 */
export function retry<T>(fn: () => Promise<T>, sleep = 500, tries = 5, max = tries): Promise<T> {
  return <Promise<T>>fn()
    .catch(e => {
      if (!--tries) {
        debugRetry(`maximum retries exceeded (${max} tries failed)`);
        debugRetry(e)
        throw e;
      }
      debugRetry(`failed, retry ${tries} times`);
      return delay(sleep)
        .then(() => retry(fn, sleep, tries, max))
    });
}

/**
 * Run async function forever. Break it with rejection.
 * @param fn Function to be called.
 */
export async function forever<T>(fn: () => Promise<T>): Promise<T> {
  debugRetry(1);
  return fn().then(() => forever(fn));
}

/**
 * Run the given function multiple times.
 * @param fn Function to be called.
 * @param sleep Time to wait in milliseconds between each run.
 * @param num Number of runs.
 */
export function times<T>(fn: () => Promise<T>, sleep = 500, num = 5): Promise<T> {
  return new Promise((resolve, reject) => {
    fn().then(r => {
      if (!--num) return resolve(r);
      debugRetry(`next run: ${num} times`);
      delay(sleep)
        .then(() => times(fn, sleep, num))
        .then(resolve);
    });
  });
}

/**
 * Run the given function whilst the async check resolves to true.
 * @param fn Function to be called.
 * @param check Function which will break if rejected or not returned `true`.
 * @param sleep Time to wait in milliseconds between each run.
 */
export async function whilst<T>(fn: () => Promise<T>, check: () => Promise<any>, sleep = 0): Promise<T> {
  return check().then(res => {
    if (!res) return Promise.reject();
    debugRetry(`check succeded, run now`);
    return doWhilst(fn, check, sleep);
  });
}

/**
 * Run the given function whilst the async check resolves to true. Run at least once.
 * @param fn Function to be called.
 * @param check Function which will break if rejected or not returned `true`.
 * @param sleep Time to wait in milliseconds between each run.
 */
export async function doWhilst<T>(fn: () => Promise<T>, check: () => Promise<any>, sleep = 0): Promise<T> {
  return fn().then(r =>
    check()
      .then(res => {
        if (!res) return Promise.reject(r);
        debugRetry(`check succeeded, next run`);
        return delay(sleep).then(() => doWhilst(fn, check, sleep));
      })
      .catch(() => r)
  );
}

/**
 * Run the given function until the async check resolves to true.
 * @param fn Function to be called.
 * @param check Function which will break if rejected or not returned `true`.
 * @param sleep Time to wait in milliseconds between each run.
 */
export async function until<T>(fn: () => Promise<T>, check: () => Promise<any>, sleep = 0): Promise<T> {
  return check()
    .catch(() => false)
    .then(res => {
      if (res) return Promise.reject();
      debugRetry(`check failed, run now`);
      return doUntil(fn, check, sleep);
    });
}

/**
 * Run the given function until the async check resolves to true. Run at least once.
 * @param fn Function to be called.
 * @param check Function which will break if rejected or not returned `true`.
 * @param sleep Time to wait in milliseconds between each run.
 */
export async function doUntil<T>(fn: () => Promise<T>, check: () => Promise<any>, sleep = 0): Promise<T> {
  return fn().then(r =>
    check()
      .catch(() => false)
      .then(res => {
        if (res) return Promise.resolve(r);
        debugRetry(`check failed, next run`);
        return delay(sleep).then(() => doUntil(fn, check, sleep));
      })
  );
}

/**
 * Calls a provided function once per input in parallel like `forEach`.
 * @param list A list of input elements to iterate.
 * @param fn An async function callback invoked for each element in the list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 */
export function each<T>(
  list: T[],
  fn: { (value: T, index: number, list: T[]): Promise<any> },
  concurrency?: number
): Promise<void> {
  return map(list, fn, concurrency).then(() => Promise.resolve());
}

/**
 * Creates a new array with the results of calling a provided function in parallel on every input.
 * The output will be in the same order as the input.
 * @param list A list of input elements to map.
 * @param fn An async function callback that produces an element of the output list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * The callback resolves to a single output element.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns A list of mapped elements in the same order as the input.
 */
export function map<T1, T2>(
  list: T1[],
  fn: { (value: T1, index: number, list: T1[]): Promise<T2> },
  concurrency?: number
): Promise<T2[]> {
  if (list.length === 0) return Promise.resolve([]) // short circuit
  let call = fn;
  if (debugMap.enabled)
    call = (value, index, list) => {
      debugMap('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
      return fn(value, index, list)
        .then(res => {
          debugMap('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
          return Promise.resolve(res);
        })
        .catch((e: any) => {
          debugMap(
            'failed %sfunction with value %o: %s',
            fn.name ? `${fn.name} ` : '',
            value,
            e.message || e
          );
          return Promise.reject(e);
        });
    };
  const limit = pLimit(concurrency || list.length);
  const queue: Promise<T2>[] = [];
  list.forEach((e, i, l) => queue.push(limit(call, e, i, l)));
  // return Promise.all(queue); // problem with multiple reject - solution use allSettled:
  return Promise.allSettled(queue)
    .then(res => {
      const errors = res.filter(e => e.status === "rejected").map((e: any) => e.reason)
      if (errors.length) return Promise.reject(errors[0]) // reject only first error
      return Promise.resolve(res.map((e: any) => e.value))
    })
}

/**
 * Creates a new object with the results of calling a provided function in parallel on every key.
 * The output will be a shallow clone.
 * @param obj An object as input map.
 * @param fn An async function callback that produces an element of the output object.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the key of the current element,
 * - and the input object.
 * The callback resolves to a single output element.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns An object like the input but with the new key values.
 */
export function mapObject(
  obj: { [key: string]: any },
  fn: { (value: any, key: string, obj: {}): Promise<any> },
  concurrency?: number
): Promise<{ [key: string]: any }> {
  obj = Object.assign({}, obj); // shallow clone
  if (Object.keys(obj).length === 0) return Promise.resolve(obj) // short circuit
  let call = fn;
  call = (value, key, list) => {
    debugMap('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
    return fn(value, key, list)
      .then(res => {
        debugMap('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
        obj[key] = res;
        return Promise.resolve(res);
      })
      .catch(e => {
        debugMap('failed %sfunction with value %o: %s', fn.name ? `${fn.name} ` : '', value, e.message || e);
        return Promise.reject(e);
      });
  };
  const keys = Object.keys(obj);
  const limit = pLimit(concurrency || keys.length);
  const queue: Promise<any>[] = [];
  keys.forEach(k => queue.push(limit(call, obj[k], k, obj)));
  // return Promise.all(queue).then(() => Promise.resolve(obj)); // problem with multiple reject - solution use allSettled:
  return Promise.allSettled(queue)
    .then(res => {
      const errors = res.filter(e => e.status === "rejected").map((e: any) => e.reason)
      if (errors.length) return Promise.reject(errors[0]) // reject only first error
      return Promise.resolve(obj);
    })
}

/**
 * Creates a new array with all elements that pass the truth test implemented by
 * the provided function in parallel.
 * The output will be in the same order as the input.
 * @param list A list of input elements to test.
 * @param fn An async function callback invoked for each element in the list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * The callback resolves to true for elements to be included in the output list.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns A list of filtered elements in the same order as the input.
 */
export function filter<T>(
  list: T[],
  fn: { (value: T, index: number, list: T[]): Promise<boolean> },
  concurrency?: number
): Promise<T[]> {
  if (list.length === 0) return Promise.resolve(list);
  let call = fn;
  if (debugFilter.enabled)
    call = (value, index, list) => {
      debugFilter('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
      return fn(value, index, list)
        .then(res => {
          debugFilter('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
          return Promise.resolve(res);
        })
        .catch(e => {
          debugFilter(
            'failed %sfunction with value %o: %s',
            fn.name ? `${fn.name} ` : '',
            value,
            e.message || e
          );
          return Promise.reject(e);
        });
    };
  const limit = pLimit(concurrency || list.length);
  const queue: Promise<T | undefined>[] = [];
  list.forEach((e, i, l) => queue.push(limit(call, e, i, l).then((v: any) => (v ? e : undefined))));
  // return Promise.all(queue).then(res => <T[]>res.filter(e => e)); // problem with multiple reject - solution use allSettled:
  return Promise.allSettled(queue)
    .then(res => {
      const errors = res.filter(e => e.status === "rejected").map((e: any) => e.reason)
      if (errors.length) return Promise.reject(errors[0]) // reject only first error
      return Promise.resolve(res.map((e: any) => e.value).filter(e => e))
    })
}

/**
 * Creates a new array with all elements that are rejected by the truth test of the
 * provided async function.
 * The output will be in the same order as the input.
 * @param list A list of input elements to test.
 * @param fn An async function callback invoked for each element in the list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * The callback resolves to false for elements to be included in the output list.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns A list of filtered elements in the same order as the input.
 */
export function reject<T>(
  list: T[],
  fn: { (value: T, index: number, list: T[]): Promise<boolean> },
  concurrency?: number
): Promise<T[]> {
  if (list.length === 0) return Promise.resolve(list);
  let call = fn;
  if (debugFilter.enabled)
    call = (value, index, list) => {
      debugFilter('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
      return fn(value, index, list)
        .then(res => {
          debugFilter('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
          return Promise.resolve(res);
        })
        .catch(e => {
          debugFilter(
            'failed %sfunction with value %o: %s',
            fn.name ? `${fn.name} ` : '',
            value,
            e.message || e
          );
          return Promise.reject(e);
        });
    };
  const limit = pLimit(concurrency || list.length);
  const queue: Promise<T | undefined>[] = [];
  list.forEach((e, i, l) => queue.push(limit(call, e, i, l).then((v: any) => (v ? undefined : e))));
  // return Promise.all(queue).then(res => <T[]>res.filter(e => e)); // problem with multiple reject - solution use allSettled:
  return Promise.allSettled(queue)
    .then(res => {
      const errors = res.filter(e => e.status === "rejected").map((e: any) => e.reason)
      if (errors.length) return Promise.reject(errors[0]) // reject only first error
      return Promise.resolve(res.map((e: any) => e.value).filter(e => e))
    })
}

/**
 * Return the first element which pass the truth test implemented by
 * the provided function in parallel.
 * @param list A list of input elements to test.
 * @param fn An async function callback invoked for each element in the list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * The callback resolves to true for elements to succeed as result.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns The value which passed the truth test as first.
 */
export function any<T>(
  list: T[],
  fn: { (value: T, index: number, list: T[]): Promise<boolean> },
  concurrency?: number
): Promise<T> {
  let call = fn;
  if (debugFilter.enabled)
    call = (value, index, list) => {
      debugFilter('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
      return fn(value, index, list)
        .then(res => {
          debugFilter('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
          return Promise.resolve(res);
        })
        .catch(e => {
          debugFilter(
            'failed %sfunction with value %o: %s',
            fn.name ? `${fn.name} ` : '',
            value,
            e.message || e
          );
          return Promise.reject(e);
        });
    };
  const queue = new pQueue({ concurrency: concurrency || list.length });
  let succeedValue: any = false;
  list.forEach((e, i, l) =>
    queue.add(() =>
      call(e, i, l).then(r => {
        if (r) {
          if (!succeedValue) succeedValue = e;
          queue.clear();
        }
      })
    )
  );
  return queue.onIdle().then(() => succeedValue);
}

/**
 * Return true if all elements pass the truth test implemented by
 * the provided function in parallel.
 * @param list A list of input elements to test.
 * @param fn An async function callback invoked for each element in the list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * The callback resolves to true for elements to succeed as result.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns The value which passed the truth test as first.
 */
export function all<T>(
  list: T[],
  fn: { (value: T, index: number, list: T[]): Promise<boolean> },
  concurrency?: number
): Promise<boolean> {
  let call = fn;
  if (debugFilter.enabled)
    call = (value, index, list) => {
      debugFilter('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
      return fn(value, index, list)
        .then(res => {
          debugFilter('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
          return Promise.resolve(res);
        })
        .catch(e => {
          debugFilter(
            'failed %sfunction with value %o: %s',
            fn.name ? `${fn.name} ` : '',
            value,
            e.message || e
          );
          return Promise.reject(e);
        });
    };
  const queue = new pQueue({ concurrency: concurrency || list.length });
  let result: boolean = true;
  list.forEach((e, i, l) =>
    queue.add(() =>
      call(e, i, l).then(r => {
        if (!r) {
          result = false;
          queue.clear();
        }
      })
    )
  );
  return queue.onIdle().then(() => result);
}

/**
 * Return true if no element pass the truth test implemented by
 * the provided function in parallel.
 * @param list A list of input elements to test.
 * @param fn An async function callback invoked for each element in the list.
 * The callback takes three arguments:
 * - the current element being processed,
 * - the index of the current element,
 * - and the input list.
 * The callback resolves to true for elements to succeed as result.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 * @returns The value which passed the truth test as first.
 */
export function none<T>(
  list: T[],
  fn: { (value: T, index: number, list: T[]): Promise<boolean> },
  concurrency?: number
): Promise<boolean> {
  let call = fn;
  if (debugFilter.enabled)
    call = (value, index, list) => {
      debugFilter('calling %sfunction with value %o', fn.name ? `${fn.name} ` : '', value);
      return fn(value, index, list)
        .then(res => {
          debugFilter('result of %sfunction with value %o is: %O', fn.name ? `${fn.name} ` : '', value, res);
          return Promise.resolve(res);
        })
        .catch(e => {
          debugFilter(
            'failed %sfunction with value %o: %s',
            fn.name ? `${fn.name} ` : '',
            value,
            e.message || e
          );
          return Promise.reject(e);
        });
    };
  const queue = new pQueue({ concurrency: concurrency || list.length });
  let result: boolean = true;
  list.forEach((e, i, l) =>
    queue.add(() =>
      call(e, i, l).then(r => {
        if (r) {
          result = false;
          queue.clear();
        }
      })
    )
  );
  return queue.onIdle().then(() => result);
}

/**
 * Calls a set of provided functions in parallel.
 * @param list A list of async function callbacks to invoke.
 * The callback takes no arguments and resolves to a void.
 * @param concurrency Limits the maximum number of functions to run concurrently.
 */
export function parallel<T = any>(list: { (): Promise<T> }[], concurrency?: number): Promise<T[]> {
  const limit = pLimit(concurrency || list.length);
  const queue: Promise<T>[] = [];
  list.forEach((fn, i) => {
    let call = fn;
    if (debugParallel.enabled)
      call = () => {
        debugParallel(
          'starting %sfunction #%d (parallel=%s)',
          fn.name ? `${fn.name} ` : '',
          i,
          concurrency || 'unlimited'
        );
        return fn()
          .then((...args) => {
            debugParallel(
              'result of %sfunction #%d is: %O',
              fn.name ? `${fn.name} ` : '',
              i,
              args.length == 1 ? args[0] : args
            );
            return Promise.resolve(...args);
          })
          .catch(e => {
            debugMap('failed %sfunction #%d: %s', fn.name ? `${fn.name} ` : '', i, e.message || e);
            return Promise.reject(e);
          });
      };
    queue.push(limit(call));
  });
  // return Promise.all(queue); // problem with multiple reject - solution use allSettled:
  return Promise.allSettled(queue)
    .then(res => {
      const errors = res.filter(e => e.status === "rejected").map((e: any) => e.reason)
      if (errors.length) return Promise.reject(errors[0]) // reject only first error
      return Promise.resolve(res.map((e: any) => e.value))
    })
}

/**
 * Calls a set of provided functions one after each other.
 * @param list A list of async function callbacks to invoke.
 * The callback takes no arguments and resolves to a void.
 */
export function serial<T = any>(list: { (): Promise<T> }[]): Promise<T[]> {
  return parallel(list, 1);
}

export default {
  // flow
  delay,
  retry,
  forever,
  times,
  whilst,
  doWhilst,
  until,
  doUntil,
  parallel,
  serial,
  // collection
  each,
  map,
  mapObject,
  filter,
  reject,
  any,
  all,
  none
};
