# Alinex Async

A collection of asynchronous helper methods. They help you to work with promise based methods to loop, filter, retry and more like with synchronous code.

A complete help with examples can be found at [alinex.gitlab.io/node-async](https://alinex.gitlab.io/node-async).

## Download Documentation

If you want to have an offline access to the documentation, feel free to download the [PDF Documentation](https://alinex.gitlab.io/node-core/core-book.pdf).

## License

(C) Copyright 2019 - 2021 Alexander Schilling

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

> <https://www.apache.org/licenses/LICENSE-2.0>

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
